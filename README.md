## Cisco Future of the Internet - React build

created with Create-React-App without ejection.

To pull node modules needed use -

### `npm install || yarn`

To run dev server use -

### `npm start || yarn start`

To Build use -

### `npm build || yarn build`

-   [Create-React-App
    documentation](https://github.com/facebook/create-react-app)
-   [ScrollMagic-React documentation](https://github.com/bitworking/react-scrollmagic)
-   [React-GSAP documentation](https://github.com/bitworking/react-gsap)
