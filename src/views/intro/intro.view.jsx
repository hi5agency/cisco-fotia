import React, { Component } from 'react'
import './intro.styles.scss'

import IntroHeadlines from '../../components/intro-headlines/intro-headlines.component'
import IntroBlocks from '../../components/intro-blocks/intro-blocks.components'

class Intro extends Component {
    render() {
        return (
            <section className="intro-wrap container">
                <IntroHeadlines mobile={this.props.mobile} />
                <IntroBlocks mobile={this.props.mobile} />
            </section>
        )
    }
}

export default Intro
