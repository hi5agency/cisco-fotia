import React, { Component } from "react";

import PILLAR_DATA from "../../data/pillar-data";

import "./pillar-tabs.styles.scss";

import PillarTabsNav from ".././../components/pillar-tabs-nav/pillar-tabs-nav.component";
import PillarTabsSlides from "../../components/pillar-tabs-slides/pillar-tabs-slides.component";

class PilarTabs extends Component {
  state = { activeID: 1, activeTab: null };
  componentDidMount() {
    this.setState({
      activeTab: PILLAR_DATA.reimagine,
      activeID: 1,
      activeSlide: 0
    });
  }
  handleClick = e => {
    let dataName = e.toLowerCase();
    console.log(e);
    this.setState({
      activeTab: PILLAR_DATA[dataName],
      activeID: PILLAR_DATA[dataName].id,
      activeSlide: 0
    });
  };
  handleSlideChange = (both, slideNum) => {
    this.setState({ activeSlide: slideNum });
  };
  handleArrowClick = (id, direction) => {
    if (direction === "next") {
      id = id + 1;
    } else {
      id = id - 1;
    }
    switch (id) {
      case 1:
        this.setState({
          activeTab: PILLAR_DATA.reimagine,
          activeID: PILLAR_DATA.reimagine.id
        });
        break;
      case 2:
        this.setState({
          activeTab: PILLAR_DATA.secure,
          activeID: PILLAR_DATA.secure.id
        });
        break;
      case 3:
        this.setState({
          activeTab: PILLAR_DATA.transform,
          activeID: PILLAR_DATA.transform.id
        });
        break;
      case 4:
        this.setState({
          activeTab: PILLAR_DATA.empower,
          activeID: PILLAR_DATA.empower.id
        });
        break;
      default:
        break;
    }
  };
  render() {
    let slideNaviClass, slidesCount, slideData, nav, slideRender, slideArrow;
    let mobileSlides = [];

    if (this.state.activeSlide !== 0) {
      slideNaviClass = "slidesNavItem active";
    } else {
      slideNaviClass = "slidesNavItem";
    }
    if (this.state.activeTab !== null) {
      slideData = this.state.activeTab.slides[0];
      slidesCount = this.state.activeTab.slides;
    } else {
      slideData = this.state.activeTab;
    }

    if (this.props.mobile) {
      mobileSlides = Object.values(PILLAR_DATA);
      mobileSlides = mobileSlides.map((slide, index) => {
        return (
          <PillarTabsSlides
            activeTab={slide.slides[0]}
            whichSlide={0}
            key={index}
            id={slide.name}
            mobile={this.props.mobile}
          />
        );
      });
      slideRender = mobileSlides;
    } else {
      slideRender = (
        <PillarTabsSlides
          activeTab={slideData}
          whichSlide={this.state.activeSlide}
        />
      );
      if (this.state.activeID === 1) {
        slideArrow = (
          <div className="arrowWrap">
            <img
              onClick={() => this.handleArrowClick(this.state.activeID, "next")}
              src="/images/pillar-tabs/right-arrow.png"
              alt=""
            />
          </div>
        );
      } else if (this.state.activeID === 4) {
        slideArrow = (
          <div className="arrowWrap">
            <img
              onClick={() => this.handleArrowClick(this.state.activeID, "back")}
              src="./images/pillar-tabs/left-arrow.png"
              alt=""
            />
          </div>
        );
      } else {
        slideArrow = (
          <div className="arrowWrap">
            <img
              onClick={() => this.handleArrowClick(this.state.activeID, "back")}
              src="./images/pillar-tabs/left-arrow.png"
              alt=""
            />
            <img
              onClick={() => this.handleArrowClick(this.state.activeID, "next")}
              src="./images/pillar-tabs/right-arrow.png"
              alt=""
            />
          </div>
        );
      }
    }
    return (
      <section className="pillar-tabs-wrap container">
        <PillarTabsNav
          handleClick={this.handleClick}
          activeID={this.state.activeID}
          mobile={this.props.mobile}
        />
        {slideRender}
        {slideArrow}
      </section>
    );
  }
}

export default PilarTabs;
