import React from 'react'
import './pillar-intro.styles.scss'

import pill1 from '../../assets/pillar-intro/pillar-intro_01.png'
import pill2 from '../../assets/pillar-intro/pillar-intro_02.png'
import pill3 from '../../assets/pillar-intro/pillar-intro_03.png'
import pill4 from '../../assets/pillar-intro/pillar-intro_04.png'

import { Controller, Scene } from 'react-scrollmagic'
import { Tween } from 'react-gsap'

const PillarIntro = ({ mobile }) => {
    let pillarIntroDesktop = (
        <section className="container pillar-intro-wrap">
            <div
                className={`${
                    mobile ? 'pillar-intro-copy-mobile' : 'pillar-intro-copy'
                } darkBlueCopy '`}
            >
                <div className="intro-copy-number">
                    <h2 className="lightBlueCopy">4</h2>
                </div>
                <div className="intro-copy-headline">
                    <h2 className="lightBlueCopy">
                        <span>
                            These <br /> imperatives for the digital
                            transformation journey
                        </span>
                    </h2>
                </div>
                <div className="pill-wrap">
                    <Controller>
                        <Scene
                            duration={300}
                            // offset={100}
                            reverse="false"
                            // indicators={true}
                        >
                            <Tween
                                wrapper={
                                    <div className="pillar-intro-animation" />
                                }
                                staggerFrom={{
                                    transform: 'translatey(160px)',
                                }}
                                stagger={0.25}
                                className="something"
                            >
                                <img src={pill1} alt="chart graphic" />
                                <img src={pill2} alt="chart graphic" />
                                <img src={pill3} alt="chart graphic" />
                                <img src={pill4} alt="chart graphic" />
                            </Tween>
                        </Scene>
                    </Controller>
                </div>
            </div>
            <div
                className={`${
                    mobile ? 'pillar-intro-end-mobile' : 'pillar-intro-end'
                }`}
            >
                <p className="lightBlueCopy">
                    open the way to a sweeping recreation of the enterprise, IT,
                    and the very role of CIO.
                </p>
            </div>
        </section>
    )
    return <div>{pillarIntroDesktop}</div>
}

export default PillarIntro
