import React from 'react'
import { Controller, Scene } from 'react-scrollmagic'
import { Tween } from 'react-gsap'

const ProofPointsDesktop = () => {
    return (
        <section className="proof-poins-wrap container">
            <div className="copy-block darkBlueCopy">
                <h2>Cisco Systems Goes Big On The ‘Next’ Internet</h2>
                <p>
                    Cisco Systems launched a strategic initiative today to bring
                    unprecedented scale to next generation networking and the
                    Internet, dubbed the “Internet for the Future.” I would like
                    to provide insights and summarize the three key components
                    of the new strategy: custom silicon, optics and software
                    enhancements.
                </p>
                <p className="lightBlueCopy">
                    <strong>
                        <em>
                            &mdash; Read more at{' '}
                            <a
                                href="https://www.forbes.com/sites/moorinsights/2019/12/11/cisco-systems-goes-big-on-the-next-internet/amp"
                                target="_blank"
                                rel="noopener noreferrer"
                                className="lightBlueCopy"
                            >
                                Forbes
                            </a>
                        </em>
                        .
                    </strong>
                </p>
            </div>
            <Controller>
                <Scene
                    duration={100}
                    offset={150}
                    enabled={true}
                    triggerElement=".proof-poins-wrap"
                    triggerHook="onCenter"
                    // indicators
                >
                    {progress => (
                        <Tween
                            to={{
                                left: '35vw',
                            }}
                            ease="Strong.easeOut"
                            totalProgress={progress}
                            paused
                        >
                            <div className="quote-block">
                                <div className="copy darkBlueCopy">
                                    <Controller>
                                        <Scene
                                            duration={100}
                                            offset={150}
                                            // indicators
                                            enabled={true}
                                            triggerElement=".proof-poins-wrap"
                                            triggerHook="onCenter"
                                        >
                                            <Tween
                                                to={{
                                                    opacity: 0,
                                                }}
                                                ease="Strong.easeOut"
                                                totalProgress={progress}
                                                paused
                                            >
                                                <p>
                                                    The three key components of
                                                    the new strategy: custom
                                                    silicon, optics and software
                                                    enhancements.
                                                </p>
                                            </Tween>
                                        </Scene>
                                    </Controller>
                                </div>
                            </div>
                        </Tween>
                    )}
                </Scene>
            </Controller>
        </section>
    )
}

export default ProofPointsDesktop
