import React from 'react'

const ProofPointsMobile = () => {
    return (
        <section className="proof-poins-wrap container">
            <div className="copy-block darkBlueCopy ">
                <h2>Cisco Systems Goes Big On The ‘Next’ Internet</h2>
                <p>
                    Cisco Systems launched a strategic initiative today to bring
                    unprecedented scale to next generation networking and the
                    Internet, dubbed the “Internet for the Future.” I would like
                    to provide insights and summarize the three key components
                    of the new strategy: custom silicon, optics and software
                    enhancements.
                </p>
                <p className="lightBlueCopy">
                    <strong>
                        <em>
                            Read more at{' '}
                            <a
                                href="https://www.forbes.com/"
                                target="_blank"
                                rel="noopener noreferrer"
                                className="lightBlueCopy"
                            >
                                Forbes
                            </a>
                        </em>
                        .
                    </strong>
                </p>
            </div>
            <div className="quote-block">
                <div className="copy darkBlueCopy">
                    <p>
                        The three key components of the new strategy: custom
                        silicon, optics and software enhancements.
                    </p>
                </div>
            </div>
        </section>
    )
}

export default ProofPointsMobile
