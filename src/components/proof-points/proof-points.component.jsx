// import React from 'react'
import React from "react";
import "./proof-points.styles.scss";

import ProofPointsDesktop from "./proof-points-desktop.components";
import ProofPointsMobile from "./proof-points-mobile.components";

const ProofPoints = props => {
  let content;
  if (props.mobile) {
    content = <ProofPointsMobile />;
  } else {
    content = <ProofPointsDesktop />;
  }
  return <div>{content}</div>;
};

export default ProofPoints;
