import React from 'react'

import { Controller, Scene } from 'react-scrollmagic'
import { Tween } from 'react-gsap'

const IntroHeadlinesDesktop = () => {
    return (
        <div className="intro-headelines">
            <Controller>
                <Scene
                    triggerElement=".app "
                    triggerHook="onLeave"
                    offset={100}
                    duration={1000}
                >
                    {progress => (
                        <Tween
                            from={{
                                opacity: 0.5,
                                top: '30px',
                                left: '20px',
                            }}
                            to={{
                                opacity: 1,
                                top: '-100px',
                                left: '40px',
                            }}
                            ease="Strong.easeOut"
                            totalProgress={progress}
                            paused
                        >
                            <h1 className="darkBlueCopy">
                                The <span>Internet</span>
                                <span>is about</span>
                                to change.
                            </h1>
                        </Tween>
                    )}
                </Scene>
                <Scene
                    triggerElement=".app"
                    triggerHook="onLeave"
                    offset={200}
                    duration={600}
                >
                    {progress => (
                        <Tween
                            from={{
                                opacity: 0.5,
                                bottom: '-30px',
                                right: '0px',
                            }}
                            to={{
                                opacity: 1,
                                bottom: '40px',
                                right: '40px',
                            }}
                            ease="Strong.easeOut"
                            totalProgress={progress}
                            paused
                        >
                            <h2>
                                CIOs need <span>to be ready.</span>
                            </h2>
                        </Tween>
                    )}
                </Scene>
            </Controller>
        </div>
    )
}

export default IntroHeadlinesDesktop
