import React from 'react'
import './intro-headlines.styles.scss'

import IntroHeadlinesDesktop from './intro-headlines-desktop.component'
import IntroHeadlinesMobile from './intro-headlines-mobile.component'

const IntroHeadlines = props => {
    let content
    if (props.mobile) {
        content = <IntroHeadlinesMobile />
    } else {
        content = <IntroHeadlinesDesktop />
    }
    return <div className="hello">{content}</div>
}

export default IntroHeadlines
