import React from "react";
import { Controller, Scene } from "react-scrollmagic";
import { Tween } from "react-gsap";
const IntroHeadlinesMobile = () => {
  return (
    <div className="intro-headelines">
      <Controller>
        <Scene
          triggerElement=".app "
          triggerHook="onLeave"
          offset={-10}
          duration={200}
          // indicators={true}
        >
          {progress => (
            <Tween
              from={{
                opacity: 0.8,
                top: "40px",
                left: "00px"
              }}
              to={{
                opacity: 1,
                top: "0px",
                left: "30px"
              }}
              ease="Strong.easeOut"
              totalProgress={progress}
              paused
            >
              <h1 className="darkBlueCopy">
                <span>The Internet is about to change.</span>
              </h1>
            </Tween>
          )}
        </Scene>
        <Scene
          triggerElement=".app"
          triggerHook="onLeave"
          offset={50}
          duration={200}
          // indicators={true}
        >
          {progress => (
            <Tween
              from={{
                opacity: 0.5,
                bottom: "10px",
                right: "10px"
              }}
              to={{
                opacity: 1,
                bottom: "40px",
                right: "20px"
              }}
              ease="Strong.easeOut"
              totalProgress={progress}
              paused
            >
              <h2>
                CIOs need <span>to be ready.</span>
              </h2>
            </Tween>
          )}
        </Scene>
      </Controller>
    </div>
  );
};

export default IntroHeadlinesMobile;
