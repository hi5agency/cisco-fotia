import React from "react";
import "./outro.styles.scss";

import { Controller, Scene } from "react-scrollmagic";
import { Tween } from "react-gsap";

const Outro = props => {
  return (
    <section className="outro-wrap container">
      <Controller>
        <Scene
          triggerElement=".outro-wrap"
          triggerHook="onLeave"
          duration={900}
          offset={-350}
          classToggle="expand"
          // indicators
        >
          {(progress, event) => <div className="outro-shape-container"></div>}
        </Scene>
      </Controller>
      <div className="outro-streaks">
        <Controller>
          <Scene
            triggerElement=".outro-wrap"
            triggerHook="onLeave"
            duration={250}
            offset={-350}
            // indicators
          >
            {progress => (
              <Tween
                to={{
                  left: "200px"
                }}
                ease="Strong.easeOut"
                totalProgress={progress}
                paused
              >
                <img
                  src="./images/outro/light-blue-streak.png"
                  alt=""
                  className="lightBlueStreak"
                />
              </Tween>
            )}
          </Scene>
          <Scene
            triggerElement=".outro-wrap"
            triggerHook="onLeave"
            duration={250}
            offset={-350}
            // indicators={true}
          >
            {progress => (
              <Tween
                to={{
                  right: "200px"
                }}
                ease="Strong.easeOut"
                totalProgress={progress}
                paused
              >
                <img
                  src="./images/outro/dark-blue-streak.png"
                  alt=""
                  className="darkBlueStreak"
                />
              </Tween>
            )}
          </Scene>
        </Controller>
      </div>
      <div className="scrolltop">
        <img
          src="./images/outro/scroll-arrow.png"
          alt=""
          onClick={props.scrollTop}
        />
      </div>
    </section>
  );
};

export default Outro;
