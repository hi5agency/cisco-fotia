import React from 'react'
import './pillar-tabs-nav.styles.scss'
import { Controller, Scene } from 'react-scrollmagic'
import { Tween } from 'react-gsap'

import AnchorLink from 'react-anchor-link-smooth-scroll'

const PillarTabsNav = props => {
    console.log(props)
    let navItems = [
        { name: 'Reimagine', active: true, id: 1 },
        { name: 'Secure', active: false, id: 2 },
        { name: 'Transform', active: false, id: 3 },
        { name: 'Empower', active: false, id: 4 },
    ]
    let navItemClass
    navItems = navItems.map(item => {
        if (props.mobile) {
            return (
                <li className="tabs-nav active" key={item.id}>
                    <AnchorLink href={`#${item.name}`}>{item.name}</AnchorLink>
                </li>
            )
        } else {
            if (props.activeID === item.id) {
                navItemClass = 'tabs-nav active'
            } else {
                navItemClass = 'tabs-nav'
            }
            return (
                <li
                    onClick={() => props.handleClick(`${item.name}`)}
                    className={navItemClass}
                    key={item.id}
                    id={item.name}
                >
                    {item.name}
                </li>
            )
        }
    })
    return (
        <div className="pillar-tab-nav">
            <Controller>
                <Scene duration={150} offset={-100} reverse="false">
                    <Tween
                        wrapper={<ul className="darkBlueCopy" />}
                        staggerFrom={{
                            opacity: 0,
                            transform: 'translatex(100px)',
                        }}
                        stagger={0.25}
                        className="something"
                    >
                        {navItems}
                    </Tween>
                </Scene>
            </Controller>
        </div>
    )
}

export default PillarTabsNav
