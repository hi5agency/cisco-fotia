import React, { Component } from 'react'
import './pillar-tabs-slides.styles.scss'
class PillarTabsSlides extends Component {
    state = {
        wrapClasses: 'pillar-tabs-slides ',
    }

    render() {
        let image, title, copy, wrapClasses, byline, link, mobileTitle
        if (this.props.activeTab) {
            image = this.props.activeTab.image
            title = this.props.activeTab.title
            copy = this.props.activeTab.copy
            byline = this.props.activeTab.byline
            if (this.props.whichSlide !== 0) {
                wrapClasses = 'pillar-tabs-slides slideAni activeBkg'
            } else {
                wrapClasses = 'pillar-tabs-slides slideAni'
            }
            if (this.props.activeTab.link) {
                link = (
                    <a
                        href={this.props.activeTab.link}
                        target="_blank"
                        rel="noopener noreferrer"
                    >
                        <span className="linkSpan">
                            See what{' '}
                            <span className="linkUnderline">{byline}</span>{' '}
                            said.
                        </span>
                    </a>
                )
            } else {
                link = <span className="lightBlueCopy">{byline}</span>
            }
        } else {
            wrapClasses = 'pillar-tabs-slides slideAni'
        }
        if (image === '/images/spacer.png') {
            wrapClasses = 'pillar-tabs-slides activeBkg secureSpecial'
        }
        setTimeout(() => {
            wrapClasses = 'pillar-tabs-slides'
        }, 3500)
        if (this.props.mobile) {
            mobileTitle = (
                <h3 id={this.props.id} className="mobileTitle">
                    {this.props.id}
                </h3>
            )
        }
        return (
            <div
                className={this.state.wrapClasses}
                key={title}
                id={this.props.id}
            >
                <div className="pts-left darkBlueCopy">
                    <h3>{title}</h3>
                    <p>{copy}</p>
                    <p>{link}</p>
                </div>
                <div className="pts-right">
                    {mobileTitle}
                    <img src={image} alt="" />
                </div>
            </div>
        )
    }
}

export default PillarTabsSlides
