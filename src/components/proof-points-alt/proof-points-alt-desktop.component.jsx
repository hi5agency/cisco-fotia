import React from 'react'
import { Controller, Scene } from 'react-scrollmagic'
import { Tween } from 'react-gsap'

const ProofPointsAltMobile = () => {
    return (
        <section className="proof-poins-wrap-alt container">
            <Controller>
                <Scene
                    duration={100}
                    offset={650}
                    enabled={true}
                    triggerElement=".proof-poins-wrap"
                    triggerHook="onCenter"
                >
                    {progress => (
                        <Tween
                            to={{
                                right: '35vw',
                            }}
                            ease="Strong.easeOut"
                            totalProgress={progress}
                            paused
                        >
                            <div className="quote-block">
                                <div className="copy darkBlueCopy">
                                    <Controller>
                                        <Scene
                                            duration={100}
                                            offset={650}
                                            enabled={true}
                                            triggerElement=".proof-poins-wrap"
                                            triggerHook="onCenter"
                                            // indicators
                                        >
                                            <Tween
                                                to={{
                                                    opacity: 0,
                                                }}
                                                ease="Strong.easeOut"
                                                totalProgress={progress}
                                                paused
                                            >
                                                <p>
                                                    The first step for any
                                                    business leader, however, is
                                                    to recognize the changing
                                                    landscape.
                                                </p>
                                            </Tween>
                                        </Scene>
                                    </Controller>
                                </div>
                            </div>
                        </Tween>
                    )}
                </Scene>
            </Controller>
            <div className="copy-block darkBlueCopy">
                <h2>
                    Businesses must prepare for ‘Hybrid IT in an unsecured
                    world’.
                </h2>
                <p>
                    Businesses aren’t spending enough on IT. Given the changing
                    demands of customers, the new expectations of regulators,
                    and the emerging competitor landscape, businesses need to
                    invest more in IT.
                </p>
                <p className="lightBlueCopy">
                    <strong>
                        <em>
                            &mdash; Read more at{' '}
                            <a
                                href="https://techwireasia.com/2019/12/cisco-chief-businesses-must-prepare-for-hybrid-it-in-an-unsecured-world/"
                                target="_blank"
                                rel="noopener noreferrer"
                                className="lightBlueCopy"
                            >
                                TechWireAsia.com
                            </a>
                        </em>
                        .
                    </strong>
                </p>
            </div>
        </section>
    )
}

export default ProofPointsAltMobile
