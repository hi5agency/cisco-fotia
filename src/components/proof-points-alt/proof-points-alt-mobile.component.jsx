import React from 'react'

const ProofPointsAltMobile = () => {
    return (
        <section className="proof-poins-wrap-alt container">
            <div className="quote-block">
                <div className="copy darkBlueCopy">
                    <p>
                        The first step for any business leader, however, is to
                        recognize the changing landscape.
                    </p>
                </div>
            </div>
            <div className="copy-block darkBlueCopy">
                <h2>
                    Businesses <br /> must prepare for ‘Hybrid IT in an
                    unsecured world’.
                </h2>
                <p>
                    Businesses aren’t spending enough on IT. Given the changing
                    demands of customers, the new expectations of regulators,
                    and the emerging competitor landscape, businesses need to
                    invest more in IT.
                </p>
                <p className="lightBlueCopy">
                    <strong>
                        <em>
                            Read more at{' '}
                            <a
                                href="https://techwireasia.com/2019/12/cisco-chief-businesses-must-prepare-for-hybrid-it-in-an-unsecured-world/"
                                target="_blank"
                                rel="noopener noreferrer"
                                className="lightBlueCopy"
                            >
                                TechWireAsia.com
                            </a>
                        </em>
                        .
                    </strong>
                </p>
            </div>
        </section>
    )
}

export default ProofPointsAltMobile
