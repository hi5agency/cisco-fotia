// import React from 'react'
import React from "react";
import "./proof-points-alt.styles.scss";

import ProofPointsAltMobile from "./proof-points-alt-mobile.component";
import ProofPointsAltDesktop from "./proof-points-alt-desktop.component";

const ProofPointsAlt = props => {
  let content;
  if (props.mobile) {
    content = <ProofPointsAltMobile />;
  } else {
    content = <ProofPointsAltDesktop />;
  }
  return <div>{content}</div>;
};
export default ProofPointsAlt;
