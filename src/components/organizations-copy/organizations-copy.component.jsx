import React from 'react'
import './organizations-copy.styles.scss'

import { Controller, Scene } from 'react-scrollmagic'
import { Tween } from 'react-gsap'

const OrganizationCopy = () => {
    return (
        <section className="organization-copy-wrap darkBlueCopy container">
            <Controller>
                <Scene duration={700} offset={-200} reverse={false}>
                    <Tween
                        wrapper={<div className="organization-copy" />}
                        staggerFrom={{
                            opacity: 0,
                            cycle: {
                                rotationX: [-90, 90],
                                transformOrigin: [
                                    '50% top -100',
                                    '50% bottom 100',
                                ],
                            },
                        }}
                        stagger={0.1}
                    >
                        <p>With the keys to transformation in place, </p>
                        <h2 className="lightBlueCopy">
                            organizations can leap forward in profound ways and
                            fully realize the promise of the Internet for the
                            Future.
                        </h2>
                        <p>
                            No longer just denizens of the data center, CIOs
                            will step into a brightly colored world. With their
                            leadership — and Cisco’s support — businesses will
                            operate faster than ever before, understand
                            customers in deep new ways, and inspire their people
                            to do amazing things.
                        </p>
                        <p>
                            The Internet is about to change. For CIOs who are
                            ready, that’s great news.
                        </p>
                    </Tween>
                </Scene>
            </Controller>
        </section>
    )
}

export default OrganizationCopy
