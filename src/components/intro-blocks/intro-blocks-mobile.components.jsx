import React from 'react'

import { Controller, Scene } from 'react-scrollmagic'
import { Tween } from 'react-gsap'

const IntroBlocksMobile = props => {
    return (
        <div className="intro-copy-wrap">
            <Controller>
                <Scene
                    duration={250}
                    pin={true}
                    enabled={true}
                    offset={-10}
                    triggerElement=".intro-copy-wrap"
                    triggerHook="onLeave"
                    // indicators={true}
                >
                    <div className="intro-copy-block-wrap">
                        <Controller>
                            <Scene
                                duration={400}
                                offset={10}
                                triggerElement=".intro-copy-wrap"
                                triggerHook="onLeave"
                                // indicators={true}
                            >
                                {progress => (
                                    <Tween
                                        to={{
                                            height: '50px',
                                        }}
                                        ease="Strong.easeOut"
                                        totalProgress={progress}
                                        paused
                                    >
                                        <div className="copy-block">
                                            <p className="regularCopy">
                                                The next wave of the Internet
                                                promises an all-new palette of
                                                possibilities. But for CIOs
                                                facing mounting security
                                                threats, widening talent gaps,
                                                and rising demands from the
                                                business, the challenges will
                                                only grow. With challenges,
                                                however, come opportunities.{' '}
                                                <em>If</em> CIOs are prepared.
                                            </p>
                                        </div>
                                    </Tween>
                                )}
                            </Scene>
                        </Controller>
                        <Controller>
                            <Scene
                                duration={300}
                                offset={-200}
                                triggerElement=".intro-copy-wrap"
                                triggerHook="onLeave"
                                // indicators={true}
                            >
                                {progress => (
                                    <Tween
                                        to={{
                                            height: '440px',
                                        }}
                                        ease="Strong.easeOut"
                                        totalProgress={progress}
                                        paused
                                    >
                                        <div className="copy-block alt">
                                            <h3 className="copyHeadline">
                                                Digital disruption is real, it
                                                is happening now, and it will
                                                affect every company and
                                                industry.
                                            </h3>
                                            <p className="regularCopy">
                                                Cisco can help — with the most
                                                cutting-edge innovations in
                                                networking, security,
                                                collaboration, and automation.
                                                Innovations that will free IT
                                                from the burdens of slow legacy
                                                infrastructures; process data
                                                and identify threats at
                                                lightning speeds; and expand
                                                IT’s creative spectrum.
                                            </p>
                                        </div>
                                    </Tween>
                                )}
                            </Scene>
                        </Controller>
                    </div>
                </Scene>
            </Controller>
        </div>
    )
}

export default IntroBlocksMobile
