import React from "react";

import IntroBlocksDesktop from "./intro-blocks-desktop.components";
import IntroBlocksMobile from "./intro-blocks-mobile.components";

const IntroBlocks = props => {
  let content;
  if (props.mobile) {
    content = <IntroBlocksMobile />;
  } else {
    content = <IntroBlocksDesktop />;
  }
  return <div>{content}</div>;
};

export default IntroBlocks;
