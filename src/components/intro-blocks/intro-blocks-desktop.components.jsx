import React from 'react'

import { Controller, Scene } from 'react-scrollmagic'
import { Tween } from 'react-gsap'

const IntroBlocksDesktop = props => {
    return (
        <div className="intro-copy-wrap">
            <Controller>
                <Scene
                    triggerElement=".intro-headelines"
                    triggerHook="onLeave"
                    offset={700}
                    duration={300}
                    // indicators
                >
                    {progress => (
                        <Tween
                            to={{
                                transform: 'scale(1.3)',
                            }}
                            ease="Strong.easeOut"
                            totalProgress={progress}
                            paused
                        >
                            <div className="intro-burst-wrapper"></div>
                        </Tween>
                    )}
                </Scene>
            </Controller>
            <Controller>
                <Scene
                    duration={50}
                    pin={true}
                    enabled={true}
                    offset={150}
                    triggerElement=".intro-copy-wrap"
                    triggerHook="onLeave"
                    // indicators={true}
                    // loglevel={3}
                >
                    <div className="intro-copy-block-wrap">
                        <Controller>
                            <Scene
                                duration={300}
                                offset={0}
                                triggerElement=".intro-copy-wrap"
                                triggerHook="onLeave"
                                // indicators={true}
                            >
                                {progress => (
                                    <Tween
                                        to={{
                                            height: '100px',
                                        }}
                                        ease="Strong.easeOut"
                                        totalProgress={progress}
                                        paused
                                    >
                                        <div className="copy-block">
                                            <p className="regularCopy">
                                                The next wave of the Internet
                                                promises an all-new palette of
                                                possibilities. But for CIOs
                                                facing mounting security
                                                threats, widening talent gaps,
                                                and rising demands from the
                                                business, the challenges will
                                                only grow. With challenges,
                                                however, come opportunities.{' '}
                                                <em>If</em> CIOs are prepared.
                                            </p>
                                        </div>
                                    </Tween>
                                )}
                            </Scene>
                        </Controller>
                        <Controller>
                            <Scene
                                duration={300}
                                offset={0}
                                triggerElement=".intro-copy-wrap"
                                triggerHook="onLeave"
                                // indicators={true}
                            >
                                {progress => (
                                    <Tween
                                        to={{
                                            height: '620px',
                                        }}
                                        ease="Strong.easeOut"
                                        totalProgress={progress}
                                        paused
                                    >
                                        <div className="copy-block alt">
                                            <h3 className="copyHeadline">
                                                Digital disruption is real, it
                                                is happening now, and it will
                                                affect every company and
                                                industry.
                                            </h3>
                                            <p className="regularCopy">
                                                Cisco can help — with the most
                                                cutting-edge innovations in
                                                networking, security,
                                                collaboration, and automation.
                                                Innovations that will free IT
                                                from the burdens of slow legacy
                                                infrastructures; process data
                                                and identify threats at
                                                lightning speeds; and expand
                                                IT’s creative spectrum.
                                            </p>
                                        </div>
                                    </Tween>
                                )}
                            </Scene>
                        </Controller>
                    </div>
                </Scene>
            </Controller>
        </div>
    )
}

export default IntroBlocksDesktop
