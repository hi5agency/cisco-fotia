import React from "react";

import ciscoLogo from "../../assets/header/CISCO-logo.png";

import "./header.styles.scss";
const Header = () => {
  return (
    <section className="container header-wrap">
      <div className="header">
        <img src={ciscoLogo} alt="CISCO logo" />
      </div>
    </section>
  );
};

export default Header;
