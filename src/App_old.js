import React from 'react'
import './App.scss'

import Header from './components/header/header.component'
import Intro from './views/intro/intro.view'
import PillarIntro from './components/pillar-intro/pillar-intro.component'
import PilarTabs from './views/pillar-tabs/pillar-tabs.view'
import OrganizationCopy from './components/organizations-copy/organizations-copy.component'
import Outro from './components/outro/outro.component'

function App() {
    return (
        <main className="app">
            <Header />
            <Intro />
            <PillarIntro />
            <PilarTabs />
            <OrganizationCopy />
            <Outro />
        </main>
    )
}

export default App
