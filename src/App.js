import React, { Component } from 'react'

import Header from './components/header/header.component'
import Intro from './views/intro/intro.view'
import ProofPoints from './components/proof-points/proof-points.component'
import ProofPointsAlt from './components/proof-points-alt/proof-points-alt.component'
import PillarIntro from './components/pillar-intro/pillar-intro.component'
import PilarTabs from './views/pillar-tabs/pillar-tabs.view'
import OrganizationCopy from './components/organizations-copy/organizations-copy.component'
import Outro from './components/outro/outro.component'

import './App.scss'
class App extends Component {
    state = {
        isMobile: false,
    }
    componentDidMount() {
        window.addEventListener('resize', this.resize)
        this.resize()
    }
    resize = () => {
        let mobileSize = window.innerWidth <= 760
        if (mobileSize !== this.state.isMobile) {
            this.setState({ isMobile: mobileSize })
        }
    }
    handleScroll = () => {
        // this.container.scrollTop = 10
        window.scrollTo(0, 0)
    }
    render() {
        return (
            <main className="app" ref={el => (this.container = el)}>
                <Header />
                <Intro mobile={this.state.isMobile} />
                <ProofPoints mobile={this.state.isMobile} />
                <ProofPointsAlt mobile={this.state.isMobile} />
                <PillarIntro mobile={this.state.isMobile} />
                <PilarTabs mobile={this.state.isMobile} />
                <OrganizationCopy />
                <Outro scrollTop={this.handleScroll} />
            </main>
        )
    }
}

export default App
