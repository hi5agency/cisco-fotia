const PILLAR_DATA = {
    reimagine: {
        name: 'Reimagine',
        id: 1,
        slides: [
            {
                title: 'Reimagine Your Apps',
                copy: `In today’s hyper-fast business environment, apps are everything. Too often, however, business apps fall short of the seamless experience of the best consumer apps — by suffering from latency, unreliability, and an inability to work with other apps, networks, and clouds. The solution? A reimagination of apps from the top down — from how they are utilized and deployed, to how they adapt to constantly changing demands. Along with how the network enables seamless, secure interactions in today’s app-heavy business  environments. `,
                byline: `NetworkWorld`,
                image: '/images/pillar-tabs/reimagine/pillar-tab-reimagine.jpg',
                link:
                    'https://www.networkworld.com/article/3487831/what-s-hot-for-cisco-in-2020.html',
            },
        ],
    },
    secure: {
        name: 'Secure',
        id: 2,
        slides: [
            {
                title: 'Secure Your Data',
                copy: `The next revolution in technology will transform the way businesses operate while supporting a more equitable and sustainable world. But its promise will come to naught if data, devices, and processes are not secure. As cybercriminals grow ever shrewder,  security teams will need to be even more capable — with deep visibility into ever-expanding network perimeters, precise detection of each and every threat, and hyper-fast responses. `,
                image: '/images/pillar-tabs/secure/pillar-tab-secure.jpg',
            },
        ],
    },
    transform: {
        name: 'Transform',
        id: 3,
        slides: [
            {
                title: 'Transform Your Infrastructure ',
                copy: `Cloud has redefined success for all organizations. But in a complex, multi-cloud world, IT is faced with vexing challenges — from processing and securing petabytes of data to enabling mobile, highly flexible workforces. So, IT teams can’t be burdened with slow, manual legacy infrastructures. Powered by machine learning and advanced automation, intent-based networking revolutionizes the way networks are managed and used. In effect, the network administrator sets the policy, and the technology “understands” the intent, monitoring, identifying, and reacting to real-time events (and threats!).`,
                byline: `CRN`,
                image: '/images/pillar-tabs/transform/pillar-tab-transform.jpg',
                link:
                    'https://www.crn.com/news/networking/aws-cisco-easing-pain-into-the-cloud-with-aws-outposts-support-sd-wan-tie-ins',
            },
        ],
    },
    empower: {
        name: 'Empower',
        id: 4,
        slides: [
            {
                title: 'Empower Your Teams',
                copy: `The more digitized companies get, the more human qualities create a winning advantage. That may seem counterintuitive. But in an age of constant disruption, creativity, collaboration, and empathy win the day. So, organizations must free their teams’ innovative spirit in all-new ways. By providing the technology to leverage data for fast decisions, collaborate across any border, and iterate ideas in diverse work cultures (and do it all securely). With the right combination of people and technology, the Inclusive Future is at hand.`,
                byline: `UCToday`,
                image: '/images/pillar-tabs/empower/pillar-tab-empower.jpg',
                link:
                    'https://www.uctoday.com/collaboration/are-you-ready-for-cognitive-collaboration/',
            },
        ],
    },
}
export default PILLAR_DATA
